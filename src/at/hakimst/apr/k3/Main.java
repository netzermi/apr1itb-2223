package at.hakimst.apr.k3;

public class Main {
    public static void main(String[] args) {
        //Ganzzahlen
        int x;
        x = 3;
        //beides in einem Schritt
        int x2 = 3;

        //Kommazahlen
        double d = 3.4;

        //Einzelne Zeichen
        char c = 'M';

        //Mehrere Zeichen/Zeichenketten
        String name = "Max";

        //Boolean
        boolean r = false && true;
        System.out.println(r);

        boolean r2 = true || true;
        System.out.println(r2);

        //umwandlung
        double d2 = 3.6;
        int di = (int) d2;
        System.out.println(di);

        int dr = (int) Math.round(d2);
        System.out.println(dr);


    }
}
